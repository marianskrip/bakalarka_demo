ifeq (run,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: install run

all: install run

install:
	pip install -r requirements/requirements.txt

install-gpu:
	pip install -r requirements/requirements-gpu.txt

run:
	python demo_test_image.py $(RUN_ARGS)
