from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
import tensorflow as tf
import time
sess = tf.InteractiveSession()

n_epochs = 25
batch_size = 50



x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])


def weight_variable(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name)


def bias_variable(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x, name=None):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name=name)


x_image = tf.reshape(x, [-1, 28, 28, 1])
keep_prob = tf.placeholder(tf.float32)

with tf.name_scope('1st_convolution_layer1') as scope:
    W_conv1 = weight_variable([5, 5, 1, 32], name="weights")
    b_conv1 = bias_variable([32], name="biases")

    h_conv1 = tf.nn.elu(conv2d(x_image, W_conv1) + b_conv1, name="convolution_activation")
    h_pool1 = max_pool_2x2(h_conv1, name="pooling")

with tf.name_scope('2nd_convolution_layer2') as scope:
    W_conv2 = weight_variable([5, 5, 32, 64], name="weights")
    b_conv2 = bias_variable([64], name="biases")

    h_conv2 = tf.nn.elu(conv2d(h_pool1, W_conv2) + b_conv2, name="convolution_activation")
    h_pool2 = max_pool_2x2(h_conv2, name="pooling")

with tf.name_scope("1st_fully_connected_layer3") as scope:
    W_fc1 = weight_variable([7 * 7 * 64, 1024], name="weights")
    b_fc1 = bias_variable([1024], name="biases")

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64], name="flattening_pool")
    h_fc1 = tf.nn.elu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1, name="fully_connected_activation")

    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob, name="dropout")

with tf.name_scope('2nd_fully_connected_layer4') as scope:
    W_fc2 = weight_variable([1024, 10], name="weights")
    b_fc2 = bias_variable([10], name="biases")

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_conv, labels=y_), name="loss")
tf.summary.scalar('loss function', cross_entropy)
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy, name="optimizer")
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1), name="correct_prediction")
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")
tf.summary.scalar('accuracy', accuracy)

merged_summary = tf.summary.merge_all()
sess.run(tf.global_variables_initializer())
file_writer = tf.summary.FileWriter('logs/', sess.graph)

saver = tf.train.Saver()
start = time.time()
for epoch in range(n_epochs):
    avg_cost = 0
    total_batch = int(mnist.train.num_examples/batch_size)
    for i in range(total_batch):
        batch = mnist.train.next_batch(batch_size)
        _, c, summary = sess.run([train_step, cross_entropy, merged_summary], feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
        file_writer.add_summary(summary, epoch * total_batch + i)
        avg_cost += c / total_batch

    if (epoch+1) % 1 == 0:
        train_accuracy = accuracy.eval(
            feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
        print("Epoch:", '%02d/%02d' % (epoch+1, n_epochs), "cost=", "{:.9f}".format(avg_cost), "training accuracy %.4f"%train_accuracy)

print("training time:%gs" % (time.time() - start))
print("test accuracy %g" % accuracy.eval(
    feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))
save_path = saver.save(sess, "saved/model.ckpt")
print("Model saved to %s" % save_path)
