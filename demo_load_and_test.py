from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
import tensorflow as tf

sess = tf.InteractiveSession()


x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])


def weight_variable(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name)


def bias_variable(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x, name=None):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name=name)

x_image = tf.reshape(x, [-1, 28, 28, 1])
keep_prob = tf.placeholder(tf.float32)


with tf.name_scope('1st_convolution_layer1') as scope:
    W_conv1 = weight_variable([5, 5, 1, 32], name="weights")
    b_conv1 = bias_variable([32], name="biases")

    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1, name="convolution_activation")
    h_pool1 = max_pool_2x2(h_conv1, name="pooling")

with tf.name_scope('2nd_convolution_layer2') as scope:
    W_conv2 = weight_variable([5, 5, 32, 64], name="weights")
    b_conv2 = bias_variable([64], name="biases")

    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2, name="convolution_activation")
    h_pool2 = max_pool_2x2(h_conv2, name="pooling")

with tf.name_scope("1st_fully_connected_layer3") as scope:
    W_fc1 = weight_variable([7 * 7 * 64, 1024], name="weights")
    b_fc1 = bias_variable([1024], name="biases")

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64], name="flattening_pool")
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1, name="fully_connected_activation")

    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob, name="dropout")

with tf.name_scope('2nd_fully_connected_layer4') as scope:
    W_fc2 = weight_variable([1024, 10], name="weights")
    b_fc2 = bias_variable([10], name="biases")

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1), name="correct_prediction")
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")

saver = tf.train.Saver()
saver.restore(sess, 'saved_relu/model.ckpt')
print('Model restored')
print("test accuracy %g" % accuracy.eval(
    feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))
