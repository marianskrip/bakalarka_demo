import threading
import tensorflow as tf

test_model = "elu"


def weight_variable(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name)


def bias_variable(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x, name=None):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name=name)


class WaitingThread(threading.Thread):


    def run(self):
        # loading image
        file_name_queue = tf.train.string_input_producer(
            ['./images/%s' % (self.image)])
        image_reader = tf.WholeFileReader()
        _, image_file = image_reader.read(file_name_queue)
        if self.image_type == "jpeg":
            image = tf.image.decode_jpeg(image_file, channels=1)
        elif self.image_type == "png":
            image = tf.image.decode_png(image_file, channels=1)
        elif self.image_type == "gif":
            image = tf.image.decode_gif(image_file, channels=1)
        else:
            raise TypeError(
                "You must specify one of the following file types: jpeg/png/gif")
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)
        self.image_tensor = self.sess.run([image])[0]
        coord.request_stop()
        # coord.join(threads)
        summ = 0
        count = 0
        maximum = 0
        # finding darkest pixel in image
        for i in range(len(self.image_tensor)):
            for j in range(len(self.image_tensor[i])):
                for k in range(len(self.image_tensor[i][j])):
                    c = self.image_tensor[i][j][k]
                    if maximum < c:
                        maximum = c
                    summ += c
                    count += 1
        avg = summ / count

        # defining edges and clearing noise
        for i in range(len(self.image_tensor)):
            for j in range(len(self.image_tensor[i])):
                for k in range(len(self.image_tensor[i][j])):
                    c = self.image_tensor[i][j][k]
                    if c > (avg-15):
                        self.image_tensor[i][j][k] = 255
                    else:
                        self.image_tensor[i][j][k] = 0


        # converting grayscale values to float
        avg = tf.constant(avg, dtype=tf.float32)
        self.image_tensor = tf.to_float(self.image_tensor, name="ToFloat")
        self.image_tensor = tf.image.resize_images(self.image_tensor,(28, 28))

        # inverting image if needed
        if avg.eval() > 127:
            print("inverting")
            mid = maximum / 2
            self.image_tensor = 255 - self.image_tensor

        with open("image.txt","w") as s:
            for k in self.image_tensor.eval():
                for p in k:
                    for i in p:
                        s.write('%3d '%i)
                s.write('\n')

        array, result = self.sess.run([self.y_conv, self.prediction], feed_dict={
                               self.x: self.image_tensor.eval(), self.keep_prob: 1.0})
        # print(array)
        print("Result: %s" % result[0])


    def __init__(self, group=None, tagret=None, name=None, args=(), kwargs={}, *, daemon=None):
        threading.Thread.__init__(self)
        self.sess = tf.InteractiveSession()
        self.x = tf.placeholder(tf.float32, shape=[28, 28, 1])
        self.keep_prob = tf.placeholder(tf.float32)
        x_image = tf.reshape(self.x, [-1, 28, 28, 1])
        print("Creating layers")
        with tf.name_scope('1st_convolution_layer1') as scope:
            W_conv1 = weight_variable([5, 5, 1, 32], name="weights")
            b_conv1 = bias_variable([32], name="biases")
            if test_model == "relu":
                h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1, name="convolution_activation")
            elif test_model == "elu":
                h_conv1 = tf.nn.elu(conv2d(x_image, W_conv1) + b_conv1, name="convolution_activation")
            h_pool1 = max_pool_2x2(h_conv1, name="pooling")
        with tf.name_scope('2nd_convolution_layer2') as scope:
            W_conv2 = weight_variable([5, 5, 32, 64], name="weights")
            b_conv2 = bias_variable([64], name="biases")
            if test_model == "relu":
                h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2, name="convolution_activation")
            elif test_model == "elu":
                h_conv2 = tf.nn.elu(conv2d(h_pool1, W_conv2) + b_conv2, name="convolution_activation")
            h_pool2 = max_pool_2x2(h_conv2, name="pooling")
        with tf.name_scope("1st_fully_connected_layer3") as scope:
            W_fc1 = weight_variable([7 * 7 * 64, 1024], name="weights")
            b_fc1 = bias_variable([1024], name="biases")
            h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64], name="flattening_pool")
            if test_model == "relu":
                h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1, name="fully_connected_activation")
            elif test_model == "elu":
                h_fc1 = tf.nn.elu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1, name="fully_connected_activation")
            h_fc1_drop = tf.nn.dropout(h_fc1, self.keep_prob, name="dropout")
        with tf.name_scope('2nd_fully_connected_layer4') as scope:
            W_fc2 = weight_variable([1024, 10], name="weights")
            b_fc2 = bias_variable([10], name="biases")
            self.y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
        self.saver = tf.train.Saver()
        if test_model == "relu":
            print("relu")
            self.saver.restore(self.sess, 'saved_relu/model.ckpt')
        elif test_model == "elu":
            print("elu")
            self.saver.restore(self.sess, 'saved/model.ckpt')
        print("Model loaded to session")
        self.image = None
        self.image_type = None
        self.prediction = tf.argmax(self.y_conv, 1)


    def test_image(self, image=None, image_type=None):
        if image is not None:
            if image_type is not None:
                self.image = image
                self.image_type = image_type
                self.run()
            else:
                raise TypeError("You must specify file type: jpeg/png/gif")
        else:
            raise FileNotFoundError("You must specify image file name")
