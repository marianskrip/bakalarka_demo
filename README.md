demo pre bakalársku prácu:

## Rozpoznávanie rukou písaného textu ##

v knižnici [TensorFlow](https://www.tensorflow.org/)

Pre nainštalovanie potrebných závislostí treba mať nainštalovaný *python 3* a *pip* a spustiť:
(make pre Windows: [gnuwin32](http://gnuwin32.sourceforge.net/packages/make.htm))


```
#!cmd

make install
```

Pre gpu verziu Tensorflow, treba vhodnú grafickú kartu a potrebné ovládače (viď [tu](https://www.tensorflow.org/get_started/os_setup#optional_install_cuda_gpus_on_linux)) a potom:


```
#!cmd

make install-gpu
```

Pre spustenie dema:


```
#!cmd

make run test_image*[0-9].jpg
```

Vstupný obrazok je v *./images/test_image.jpg* stačí ho zmeniť (nie veľkosť, musí byť 28x28 a byť biele na čiernom, taký bol dataset, s ktorým som neurónovú sieť trénoval)

V zložke *./images/* je takisto pár mnou predkreslených cifier na otestovanie 

Pre použitie vlastného obrázka, ktorý ma iný formát ako *jpeg* (*png, gif*), treba špecifikovať aj jeho typ, napr:

```
#!cmd

make run image.png png
```