import threading
from waiting_thread import WaitingThread
import sys

try:
    image = sys.argv[1]
except IndexError:
    print("You have to specify file, for example: test_image[0-9].jpg")
try:
    image_type = sys.argv[2]
except IndexError:
    image_type = "jpeg"
thread = WaitingThread(name="waiting_thread")

def test_image(n, t):
    thread.test_image(image=n ,image_type=t)

test_image(n=image ,t=image_type)
